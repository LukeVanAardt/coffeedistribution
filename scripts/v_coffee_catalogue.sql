USE [CoffeeDistribution]
GO

CREATE VIEW vCoffeeCatalogue
AS
    SELECT [CoffeeType], [Varietal], [HarvestMethodName], [ProcessMethodName], [RegionName], [Price]
    FROM [dbo].[Coffee] AS cof
        INNER JOIN [dbo].[Varietal] AS [var]
        ON cof.[VarietalID] = [var].[VarietalID]
        INNER JOIN [dbo].[HarvestMethod] AS har
        ON cof.[HarvestMethodID] = har.[HarvestMethodID]
        INNER JOIN [dbo].[ProcessMethod] AS pro
        ON cof.[ProcessMethodID] = pro.[ProcessMethodID]
        INNER JOIN [dbo].[Farm] AS far
        ON cof.[FarmID] = far.[FarmID]
        INNER JOIN [dbo].[Region] AS reg
        ON far.[RegionID] = reg.[RegionID]
        INNER JOIN [dbo].[Pricing] AS pri
        ON cof.[CoffeeID] = pri.[CoffeeID]
        INNER JOIN [dbo].[CoffeeType] AS typ
        ON cof.[CoffeeTypeID] = typ.[CoffeeTypeID];
GO
