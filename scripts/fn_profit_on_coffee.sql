USE [CoffeeDistribution];
GO

ALTER FUNCTION getProfitOnCoffee(@coffeeId INT)
RETURNS FLOAT
AS
BEGIN
    DECLARE @costPrice FLOAT = (SELECT [Cost] FROM [dbo].[ImportOrderItem] io WHERE io.CoffeeId = @coffeeId);
    DECLARE @grossIncome FLOAT = [dbo].getGrossIncomeOnCoffee(@coffeeId);
    DECLARE @profit FLOAT = @grossIncome - @costPrice;

    RETURN  @profit;
END