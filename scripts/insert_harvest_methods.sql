USE [CoffeeDistribution];
GO

INSERT INTO [CoffeeDistribution].[dbo].[HarvestMethod]
    VALUES
    (
        'Strip Harvesting'
    ),
    (
        'Selective Harvesting'
    );