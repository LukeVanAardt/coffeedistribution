USE [CoffeeDistribution];
GO

INSERT INTO [CoffeeDistribution].[dbo].[OrderStatus]
    VALUES
    (
        'Delivered'
    ),
    (
        'Cancelled'
    ),
    (
        'Declined'
    ),
    (
        'Disputed'
    ),
    (
        'Cancelled'
    ),
    (
        'Picked Up'
    ),
    (
        'Processing'
    );