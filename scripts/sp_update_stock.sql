USE [CoffeeDistribution];
GO

ALTER PROCEDURE [dbo].[sp_updateStock]
    @stockId        INT,
    @updateValue    FLOAT = 0,
    @updateFlag     INT = 0
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @temp FLOAT;
    SET @temp = (SELECT [Quantity] FROM [dbo].[Stock] WHERE [StockId] = @stockId)

    IF @updateFlag = 0
        UPDATE  [Stock]
            SET [Quantity] = @temp - @updateValue
    ELSE IF @updateFlag = 1
        UPDATE [Stock]
            SET [Quantity] = @temp + @updateValue
END;