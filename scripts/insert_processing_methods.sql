USE [CoffeeDistribution];
GO

INSERT INTO [CoffeeDistribution].[dbo].[ProcessMethod]
    VALUES
    (
        'Wet'
    ),
    (
        'Dry'
    ),
    (
        'Semi-Dry'
    );