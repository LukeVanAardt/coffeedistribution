use CoffeeDistribution
GO

CREATE PROCEDURE [dbo].[spCreateCoffee]
@CoffeeType [nvarchar](120),
@Varietal [nvarchar](120),
@HarvestMethodName [nvarchar](120),
@ProcessMethodName [nvarchar](120),
@FarmID [int]

AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @harvestMethodID int
			DECLARE @processMethodID int
			
			INSERT INTO HarvestMethod(harvestmethodname) 
			VALUES (@HarvestMethodName)
			SELECT @harvestMethodID = SCOPE_IDENTITY()
			INSERT INTO ProcessMethod(ProcessMethodName) 
			VALUES (@ProcessMethodName)
			SELECT @processMethodID = SCOPE_IDENTITY()
			
			INSERT INTO Coffee(
				CoffeeType, 
				Varietal, 
				HarvestMethodID, 
				ProcessMethodID, 
				FarmID
			) 
			VALUES (
				@CoffeeType, 
				@Varietal,
				@harvestMethodID,
				@processMethodID, 
				@FarmID
			)
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		PRINT 'Could not process transaction'
	END CATCH
END