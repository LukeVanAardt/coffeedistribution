USE [CoffeeDistribution]
GO

CREATE VIEW vPendingImport
AS
SELECT [SupplierName],[CoffeeType],[Varietal],
[Quantity],[Cost],[OrderDate],[ArrivalDate]
FROM [dbo].[Supplier] AS sup 
INNER JOIN [dbo].[ImportOrder] AS [io]
ON sup.SupplierID = [io].SupplierID
INNER JOIN [dbo].[ImportOrderItem] AS ioi
ON [io].OrderID = ioi.ImportOrderID
INNER JOIN [dbo].[Coffee] AS cof
ON ioi.CoffeeID = cof.CoffeeID
INNER JOIN [dbo].[CoffeeType] AS typ
ON cof.CoffeeTypeID = typ.CoffeeTypeID
INNER JOIN [dbo].[Varietal] AS [var]
ON cof.VarietalID = [var].VarietalID
INNER JOIN [dbo].[OrderStatus] AS os
ON [io].OrderStatusID = os.OrderStatusID
WHERE os.[StatusName] = 'pending';
GO