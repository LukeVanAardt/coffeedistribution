USE [CoffeeDistribution]
GO

CREATE VIEW vPendingExports
AS
SELECT cli.[ClientID],[CoffeeType],[Varietal],
[Quantity],[Price],[OrderDate],[DeliveryDate]
FROM [dbo].[Client] AS cli 
INNER JOIN [dbo].[ExportOrder] AS eo
ON cli.ClientID = eo.ClientID
INNER JOIN [dbo].[ExportOrderItem] AS eoi
ON eo.OrderID = eoi.ExportOrderID
INNER JOIN [dbo].[Coffee] AS cof
ON eoi.CoffeeID = cof.CoffeeID
INNER JOIN [dbo].[CoffeeType] as typ
ON cof.[CoffeeTypeID] = typ.[CoffeeTypeID]
INNER JOIN [dbo].[Varietal] AS [var]
ON cof.VarietalID = [var].VarietalID
INNER JOIN [dbo].[OrderStatus] AS os
ON eo.OrderStatusID = os.OrderStatusID
WHERE os.[StatusName] = 'processing';
GO
