USE [CoffeeDistribution];
GO

CREATE FUNCTION getGrossIncomeOnCoffee(@coffeeId INT)
RETURNS FLOAT
AS
BEGIN
    DECLARE @grossIncome FLOAT = (SELECT SUM(price) FROM ExportOrderItem WHERE ExportOrderItem.CoffeeId = @coffeeId);

    RETURN @grossIncome
END;