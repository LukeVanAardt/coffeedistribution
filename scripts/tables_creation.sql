USE [CoffeeDistribution];
GO

BEGIN TRANSACTION t_create_database

BEGIN TRY

CREATE TABLE [CoffeeDistribution].[dbo].[HarvestMethod]
(
	HarvestMethodID INT IDENTITY(1,1) PRIMARY KEY,
	HarvestMethodName VARCHAR(250) NOT NULL
);

CREATE TABLE [CoffeeDistribution].[dbo].[ProcessMethod]
(
	ProcessMethodID INT IDENTITY(1,1) PRIMARY KEY,
	ProcessMethodName VARCHAR(250) NOT NULL
);

CREATE TABLE [CoffeeDistribution].[dbo].[ShippingMethod]
(
	ShippingMethodID INT IDENTITY(1,1) PRIMARY KEY,
	ShippingMethodName VARCHAR(250) NOT NULL
);

CREATE TABLE [CoffeeDistribution].[dbo].[Region]
(
	RegionID INT IDENTITY(1,1) PRIMARY KEY,
	RegionName NVARCHAR(250)
);

CREATE TABLE Supplier
(
	SupplierID INT IDENTITY(1,1) PRIMARY KEY,
	SupplierName NVARCHAR(255),
	SupplierContactNumber NVARCHAR(255),
	SupplierEmailAddress NVARCHAR(255)
);

CREATE TABLE [CoffeeDistribution].[dbo].[Farm]
(
	FarmID INT IDENTITY(1,1) PRIMARY KEY,
	FarmName NVARCHAR(250),
	AverageAltitude FLOAT,
	RegionID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Region](RegionID),
	HarvestStartDate DateTime,
	HarvestEndDate DateTime,
	SupplierId INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Supplier](SupplierID)
);

CREATE TABLE [CoffeeDistribution].[dbo].[FlavourNotes]
(
	FlavourNotesID INT IDENTITY(1,1) PRIMARY KEY,
	Flavour VARCHAR(250)
);

CREATE TABLE [CoffeeDistribution].[dbo].[Varietal]
(
	VarietalID INT IDENTITY(1,1) PRIMARY KEY,
	Varietal VARCHAR(250)
);

CREATE TABLE [CoffeeDistribution].[dbo].[CoffeeType]
(
	CoffeeTypeID INT IDENTITY(1,1) PRIMARY KEY,
	CoffeeType VARCHAR(250)
);

CREATE TABLE [CoffeeDistribution].[dbo].[Coffee]
(
	CoffeeID INT IDENTITY(1,1) PRIMARY KEY,
	HarvestMethodID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[HarvestMethod](HarvestMethodID),
	ProcessMethodID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[ProcessMethod](ProcessMethodID),
	CoffeeTypeID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[CoffeeType](CoffeeTypeID),
	VarietalID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Varietal](VarietalID),
	FarmID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Farm](FarmID)
);

CREATE TABLE [CoffeeDistribution].[dbo].[CoffeeFlavour]
(
	CoffeeFlavourID INT IDENTITY(1,1) PRIMARY KEY,
	CoffeeID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Coffee],
	FlavourNotesID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[FlavourNotes](FlavourNotesID) NOT NULL
);

CREATE TABLE [CoffeeDistribution].[dbo].[OrderStatus]
(
	OrderStatusID INT IDENTITY(1,1) PRIMARY KEY,
	StatusName NVARCHAR(255) NOT NULL
);

CREATE TABLE [CoffeeDistribution].[dbo].[ImportOrder]
(
	OrderID INT IDENTITY(1,1) PRIMARY KEY,
	SupplierID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Supplier](SupplierID),
	OrderDate DATETIME,
	ArrivalDate DATETIME,
	FarmID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Farm](FarmID),
	OrderStatusID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[OrderStatus](OrderStatusID),
	ShippingMethodID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[ShippingMethod](ShippingMethodID)
);

CREATE TABLE [CoffeeDistribution].[dbo].[ImportOrderItem]
(
	ItemID INT IDENTITY(1,1) PRIMARY KEY,
	Quantity INT,
	Cost MONEY,
	CoffeeID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Coffee](CoffeeID),
	ImportOrderID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[ImportOrder](OrderID)
);

CREATE TABLE [CoffeeDistribution].[dbo].[Stock]
(
	StockID INT IDENTITY(1,1) PRIMARY KEY,
	CoffeeID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Coffee](CoffeeID),
	Quantity INT
);

CREATE TABLE [CoffeeDistribution].[dbo].[Address]
(
	AddressID INT IDENTITY(1,1) PRIMARY KEY,
	AddressLine1 NVARCHAR(255),
	AddressLine2 NVARCHAR(255),
	AddressLine3 NVARCHAR(255)
);

CREATE TABLE [CoffeeDistribution].[dbo].[Client]
(
	ClientID INT IDENTITY(1,1) PRIMARY KEY,
	AddressID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Address](AddressID),
	PhoneNumber NVARCHAR(20),
	EmailAddress NVARCHAR(250)
);	

CREATE TABLE [CoffeeDistribution].[dbo].[ExportOrder]
(
	OrderID INT IDENTITY(1,1) PRIMARY KEY,
	OrderDate DATETIME,
	DeliveryDate DATETIME,
	OrderStatusID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[OrderStatus](OrderStatusID),
	ClientID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Client](ClientID)
);

CREATE TABLE [CoffeeDistribution].[dbo].[ExportOrderItem]
(
	ItemID INT IDENTITY(1,1) PRIMARY KEY,
	Quantity INT,
	Price MONEY,
	ExportOrderID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[ExportOrder](OrderID),
	CoffeeID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Coffee](CoffeeID)
);

CREATE TABLE [CoffeeDistribution].[dbo].[Pricing]
(
	PricingID INT IDENTITY(1,1) PRIMARY KEY,
	CoffeeID INT FOREIGN KEY REFERENCES [CoffeeDistribution].[dbo].[Coffee](CoffeeID),
	Price MONEY
);

COMMIT

END TRY

BEGIN CATCH

SELECT
	ERROR_NUMBER() AS ErrorNumber,
	ERROR_STATE() AS ErrorState,
	ERROR_SEVERITY() AS ErrorSeverity,
	ERROR_PROCEDURE() AS ErrorProcedure,
	ERROR_LINE() AS ErrorLine,
	ERROR_MESSAGE() AS ErrorMessage;

ROLLBACK

END CATCH