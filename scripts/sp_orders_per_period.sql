USE [CoffeeDistribution];
GO

CREATE PROCEDURE [dbo].[sp_GetOrdersForPeriod]
    @startDate DATETIME,
    @endDate DATETIME
AS
BEGIN
    SET NOCOUNT ON;
    SELECT COUNT(OrderId) FROM [dbo].[ExportOrder] WHERE [OrderDate] BETWEEN @startDate AND @endDate;
END;